package com.github.alvarosct02.democes.features;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.github.alvarosct02.democes.R;
import com.github.alvarosct02.democes.data.Item;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupView();
    }

    private void setupView() {

//        Check JAVA 8 Lambdas
        findViewById(R.id.bt_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ViewItemActivity.newInstance(MainActivity.this, new Item("Prueba CES")));
            }
        });

        findViewById(R.id.bt_new).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(NewItemActivity.newInstance(MainActivity.this));
            }
        });
    }
}
