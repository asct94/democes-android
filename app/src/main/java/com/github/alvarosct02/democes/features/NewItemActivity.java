package com.github.alvarosct02.democes.features;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.alvarosct02.democes.R;
import com.github.alvarosct02.democes.data.Item;

public class NewItemActivity extends AppCompatActivity {

    public static Intent newInstance(Context context){
        Intent i = new Intent(context, NewItemActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_item);
        setTitle("Nuevo Item");
    }
}
