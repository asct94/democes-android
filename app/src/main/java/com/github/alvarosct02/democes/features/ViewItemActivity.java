package com.github.alvarosct02.democes.features;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.github.alvarosct02.democes.R;
import com.github.alvarosct02.democes.data.Item;
import com.github.alvarosct02.democes.utils.Constants;
import com.google.gson.Gson;

public class ViewItemActivity extends AppCompatActivity {

    public static Intent newInstance(Context context, Item item){
        Intent i = new Intent(context, ViewItemActivity.class);

//        Serialization. Also check for Parcelable
        String itemStr = new Gson().toJson(item, Item.class);
        i.putExtra(Constants.EXTRA_ITEM, itemStr);

        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);
        setTitle("Consultar Item");

//        Get passed item
        String itemStr = getIntent().getStringExtra(Constants.EXTRA_ITEM);
        Item item = new Gson().fromJson(itemStr, Item.class);

//        Setup View
        TextView tvName = findViewById(R.id.tv_name);
        tvName.setText(item.getName());

    }
}
